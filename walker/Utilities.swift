/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Core Graphics utility extensions used in the sample code.
*/

import UIKit
import CoreGraphics
import ImageIO

extension CGPoint {
    func scaled(to size: CGSize) -> CGPoint {
        return CGPoint(x: self.x * size.width, y: self.y * size.height)
    }
}
extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: self.origin.x * size.width,
            y: self.origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}

extension CGImagePropertyOrientation {
    init(_ orientation: UIImageOrientation) {
        switch orientation {
        case .up: self = .up
        case .upMirrored: self = .upMirrored
        case .down: self = .down
        case .downMirrored: self = .downMirrored
        case .left: self = .left
        case .leftMirrored: self = .leftMirrored
        case .right: self = .right
        case .rightMirrored: self = .rightMirrored
        }
    }
}

extension UIColor {
    
    public class func jtBlue() -> UIColor {
        return UIColor(red:0.33, green:0.78, blue:0.99, alpha:1.00)
    }
    
    public class func jtPhone() -> UIColor {
        return UIColor(red:1.00, green:0.16, blue:0.32, alpha:1.00)
    }
    
    public class func jtLink() -> UIColor {
        return UIColor(red:0.49, green:0.83, blue:0.13, alpha:1.00)
    }
    
    public class func jtEmail() -> UIColor {
        return UIColor(red:0.00, green:0.46, blue:1.00, alpha:1.00)
    }
    
    public class func jtMap() -> UIColor {
        return UIColor(red:0.96, green:0.65, blue:0.14, alpha:1.00)
    }
    
    public class func colorFromHex(rgbValue:UInt32)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}


