//
//  SettingsCollectionViewController.swift
//  walker
//
//  Created by John Tran on 8/3/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit


protocol SettingsControllerDelegate: class {
    func scanTextWithPhotoLibrary(_ controller: SettingsCollectionViewController)
    func contactUs(_ controller: SettingsCollectionViewController)
    func rateTheApp(_ controller: SettingsCollectionViewController)
    func facebook(_ controller: SettingsCollectionViewController)
    func twitter(_ controller: SettingsCollectionViewController)
    func visitWebsite(_ controller: SettingsCollectionViewController)
}

private let reuseIdentifier = "SettingCell"

class SettingsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    weak var delegate: SettingsControllerDelegate?
    
    let settings = ["Twitter" : "twitter",
                    "Facebook" : "facebook",
                    "Contact Us" : "email",
                    "Rate Our App" : "star",
                    "Scan From Photo Library" : "photoLibrary",
                    "Visit Our Site" : "link"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
//        let closeBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
//        self.navigationItem.leftBarButtonItem = closeBtn
    }

    @objc
    func close() {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settings.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SettingCell
    
        let item = self.settings[indexPath.item]
        cell.name.text = item.key
        cell.icon.image = UIImage(named: item.value)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = 5.0
        let column = 3.0
        let width = (self.view.bounds.width - CGFloat(padding * (column + 1))) / CGFloat(column)
        let height = width * 0.8
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.settings[indexPath.item]
        
        switch item.value {
        case "photoLibrary":
            delegate?.scanTextWithPhotoLibrary(self)
        case "star":
            delegate?.rateTheApp(self)
        case "email":
            delegate?.contactUs(self)
        case "twitter":
            delegate?.twitter(self)
        case "facebook":
            delegate?.facebook(self)
        case "link":
            delegate?.visitWebsite(self)
        default:
            print("weird option")
        }
        
    }
    

}

extension Dictionary {
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }
}
