//
//  TextCollectionViewController.swift
//  walker
//
//  Created by John Tran on 8/2/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import CoreData
import SugarRecord

private let reuseIdentifier = "TextRecordCollectionCell"

class RecordCollectionViewController: UICollectionViewController, CollectionViewWaterfallLayoutDelegate {

    lazy var db: CoreDataDefaultStorage = {
        let store = CoreDataStore.named("walker")
        let bundle = Bundle(for: RecordCollectionViewController.classForCoder())
        let model = CoreDataObjectModel.merged([bundle])
        let defaultStorage = try! CoreDataDefaultStorage(store: store, model: model)
        return defaultStorage
    }()
    
    lazy var recordObservable: RequestObservable<TextRecord> = {
        let request = FetchRequest<TextRecord>().sorted(with: "date", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var structureDataObservable: RequestObservable<StructuredData> = {
        let request = FetchRequest<StructuredData>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var addressObservable: RequestObservable<Address> = {
        let request = FetchRequest<Address>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    var textList: [TextRecord] = []
    
    var shouldReload = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Collected Text"

        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.headerInset = UIEdgeInsetsMake(5, 0, 0, 0)
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        self.collectionView!.collectionViewLayout = layout

        self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
        self.setupObservable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupObservable() {
        recordObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("text record changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        structureDataObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("structured data changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        addressObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("addresses changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
    }
    
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.textList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TextRecordCollectionCell
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        let aRecord = self.textList[indexPath.row]
        DispatchQueue.main.async {
            let image = UIImage(data: aRecord.thumbNail!)
            DispatchQueue.main.async {
                cell.capturedImage.image = image
            }
        }
        cell.capturedText?.text = aRecord.content
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        let aRecord = self.textList[indexPath.row]
        let textVC = self.storyboard?.instantiateViewController(withIdentifier: "TextViewController") as! RecordViewController
        textVC.aTextRecord = aRecord
        
        textVC.cancelCompletionHandler = { (optionalRecord: TextRecord?) -> Void in
            self.dismiss(animated: true, completion: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if self.shouldReload {
                        self.collectionView?.reloadData()
                    }
                })
            })
        }
        
        let navController = UINavigationController.init(rootViewController: textVC)
        
        if let textCollectionCell = collectionView.cellForItem(at: indexPath) as? TextRecordCollectionCell {
            navController.cc_setZoomTransition(originalView: textCollectionCell.contentView)
        }
        
        self.present(navController, animated: true, completion: nil)
        
        return false
    }
    
    
    // MARK: WaterfallLayoutDelegate
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let aRecord = self.textList[indexPath.row]
        
        let padding = 10.0
        let column = 2.0
        let width = (self.view.bounds.width - CGFloat(padding * (column + 1))) / CGFloat(column)
        
        let messageString = aRecord.content
        let attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString!, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        // when text is too long
        var height = (rect.height <= 250.0) ? rect.height : 250.0
        
        // when text is just one liner
        if height <= 110.0 {
            height = 110.0
        }
        
        return CGSize(width: width, height: height)
    }

}
