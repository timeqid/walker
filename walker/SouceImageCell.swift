//
//  SouceImageCell.swift
//  walker
//
//  Created by John Tran on 9/17/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit

class SouceImageCell: UITableViewCell {

    @IBOutlet weak var sourceImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
