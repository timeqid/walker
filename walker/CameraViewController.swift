//
//  CameraViewController.swift
//  walker
//
//  Created by John Tran on 7/25/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import AVFoundation
import SugarRecord
import CoreData
import Photos
import Vision
import TesseractOCR
import Jelly
import StarWars
import MessageUI
import APESuperHUD
import Hero
import SCLAlertView
import SafariServices
import PAPermissions

func loadShutterSoundPlayer() -> AVAudioPlayer? {
    let theMainBundle = Bundle.main
    let filename = "Shutter sound"
    let fileType = "mp3"
    let soundfilePath: String? = theMainBundle.path(forResource: filename,
                                                    ofType: fileType,
                                                    inDirectory: nil)
    if soundfilePath == nil
    {
        return nil
    }
    //println("soundfilePath = \(soundfilePath)")
    let fileURL = URL(fileURLWithPath: soundfilePath!)
    var error: NSError?
    let result: AVAudioPlayer?
    do {
        result = try AVAudioPlayer(contentsOf: fileURL)
    } catch let error1 as NSError {
        error = error1
        result = nil
    }
    if let requiredErr = error
    {
        print("AVAudioPlayer.init failed with error \(requiredErr.debugDescription)")
    }
    if result?.settings != nil
    {
        //println("soundplayer.settings = \(settings)")
    }
    result?.prepareToPlay()
    return result
}

enum StructuredDataType: String {
    case address = "Address"
    case email = "Email"
    case website = "Website"
    case phone = "Phone"
    case plainText = "PlainText"
}

class CameraViewController: UIViewController, G8TesseractDelegate, CropViewControllerDelegate,
                            UINavigationControllerDelegate, UIImagePickerControllerDelegate,
                            UIViewControllerTransitioningDelegate, SettingsControllerDelegate,
                            MFMailComposeViewControllerDelegate, SFSafariViewControllerDelegate, PAPermissionsViewControllerDelegate{
    
    var shutterSoundPlayer = loadShutterSoundPlayer()
    
    var tesseract:G8Tesseract = G8Tesseract(language:"eng");
    
    var jellyAnimator: JellyAnimator?
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var settingsButton: UIButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    lazy var db: CoreDataDefaultStorage = {
        let store = CoreDataStore.named("walker")
        let bundle = Bundle(for: CameraViewController.classForCoder())
        let model = CoreDataObjectModel.merged([bundle])
        let defaultStorage = try! CoreDataDefaultStorage(store: store, model: model)
        return defaultStorage
    }()
    
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var scanListBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkPermissionFromBackgrounding), name: .UIApplicationDidBecomeActive, object: nil)
        
        self.settingsBtn.layer.cornerRadius = self.settingsBtn.bounds.size.height/2
        self.photoButton.layer.cornerRadius = self.photoButton.bounds.size.height/2
        self.scanListBtn.layer.cornerRadius = self.scanListBtn.bounds.size.height/2
        
        togglePreviewImage(hidden: true, image: nil)
        
        //----------- setup OCR
        self.tesseract.delegate = self;
        self.tesseract.engineMode = .tesseractCubeCombined
        self.tesseract.pageSegmentationMode = .auto
        self.tesseract.maximumRecognitionTime = 60.0
        
        // HUD drawing
        APESuperHUD.appearance.cornerRadius = 12
        APESuperHUD.appearance.animateInTime = 0.3
        APESuperHUD.appearance.animateOutTime = 0.3
        APESuperHUD.appearance.backgroundBlurEffect = .extraLight
        APESuperHUD.appearance.cancelableOnTouch = false
        APESuperHUD.appearance.iconWidth = 48
        APESuperHUD.appearance.iconHeight = 48
        APESuperHUD.appearance.titleFontSize = 22
        APESuperHUD.appearance.messageFontSize = 18
 
        // check permission
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            self.setupCameraFeed()
        case .notDetermined:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PermissionsViewController") as! PermissionsViewController
            controller.delegate = self
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: false, completion: nil)
            setupResult = .notAuthorized
        case .denied:
            let alertView = self.permissionAlert()
            alertView.showInfo("FYI", subTitle: "Smart Text needs camera permission to work correctly")
            setupResult = .notAuthorized
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                self.addObservers()
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
                
            case .notAuthorized:
                print("Not authorized")
            case .configurationFailed:
                //break
                DispatchQueue.main.async { [unowned self] in
                    let alertMsg = "Failure to setup camera feed"
                    let message = NSLocalizedString("Failure to setup camera feed", comment: alertMsg)
                    let alertController = UIAlertController(title: "Smart Text", message: message, preferredStyle: .alert)

                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"),
                                                            style: .cancel,
                                                            handler: nil))

                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        
        sessionQueue.async { [unowned self] in
            if self.setupResult == .success {
                self.session.stopRunning()
                self.isSessionRunning = self.session.isRunning
                self.removeObservers()
            }
        }
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                return
            }
            
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }
    
    @objc func checkPermissionFromBackgrounding() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            if setupResult == .success {
                self.sessionQueue.async {
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                }
            }
            self.toggleCameraLabel(hidden: true)
        case .notDetermined:
            setupResult = .notAuthorized
            self.toggleCameraLabel(hidden: false)
        case .denied:
            setupResult = .notAuthorized
            self.toggleCameraLabel(hidden: false)
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
            self.toggleCameraLabel(hidden: false)
        }
    }
    // MARK: - Convenience methods
    func setupCameraFeed() {
        // Set up the video preview view.
        previewView.session = session
        
        /*
         Setup the capture session.
         In general it is not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.
         
         Why not do all of this on the main queue?
         Because AVCaptureSession.startRunning() is a blocking call which can
         take a long time. We dispatch session setup to the sessionQueue so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async { [unowned self] in
            self.configureSession()
        }
    }
    
    func toggleCameraLabel(hidden: Bool) {
        if hidden {
            UIView.animate(withDuration: 0.25,
                           animations: { [unowned self] in
                            self.cameraUnavailableLabel.alpha = 0
                            self.settingsButton.alpha = 0
                }, completion: { [unowned self] _ in
                    self.cameraUnavailableLabel.isHidden = true
                    self.settingsButton.isHidden = true
                }
            )
        } else if cameraUnavailableLabel.isHidden {
            settingsButton.alpha = 0
            settingsButton.isHidden = false
            cameraUnavailableLabel.alpha = 0
            cameraUnavailableLabel.isHidden = false
            UIView.animate(withDuration: 0.25) { [unowned self] in
                self.cameraUnavailableLabel.alpha = 1
                self.settingsButton.alpha = 1
            }
        }
    }
    
    func permissionAlert() -> SCLAlertView
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            contentViewCornerRadius: CGFloat(15),
            buttonCornerRadius: CGFloat(7),
            hideWhenBackgroundViewIsTapped: true
        )
        
        let alertView = SCLAlertView(appearance:appearance)
        alertView.addButton("OK", action: {
            alertView.hideView()
        })
        alertView.addButton("Permissions", action: {
            alertView.hideView()
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PermissionsViewController") as! PermissionsViewController
            controller.delegate = self
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
        })
        
        return alertView
    }
    
    @IBAction func openSettings() {
        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.open(settingsURL!, options: [:], completionHandler: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SettingVC") {
            let navViewController = segue.destination as! UINavigationController
            //1.
            let presentation = JellyShiftInPresentation(dismissCurve: JellyConstants.JellyCurve.linear, presentationCurve: JellyConstants.JellyCurve.linear, cornerRadius: 10.0, backgroundStyle: JellyConstants.BackgroundStyle.blur(effectStyle: .regular), jellyness: JellyConstants.Jellyness.jelly, duration: JellyConstants.Duration.normal, direction: JellyConstants.Direction.bottom, size: JellyConstants.Size.custom(value: 210.0), corners: [.topLeft, .topRight, .bottomLeft, .bottomRight])
            //2.
            self.jellyAnimator = JellyAnimator(presentation:presentation)
            //3.
            self.jellyAnimator?.prepare(viewController: navViewController)
            
            let viewController = navViewController.topViewController as! SettingsCollectionViewController
            viewController.delegate = self
        }
        else if (segue.identifier == "TextRecordList") {
            
            let navViewController = segue.destination as! UINavigationController
            
            let presentation = JellyShiftInPresentation(dismissCurve: JellyConstants.JellyCurve.linear, presentationCurve: JellyConstants.JellyCurve.linear, cornerRadius: 0.0, backgroundStyle: JellyConstants.BackgroundStyle.blur(effectStyle: .regular), jellyness: JellyConstants.Jellyness.none, duration: JellyConstants.Duration.medium, direction: JellyConstants.Direction.bottom, size: JellyConstants.Size.fullscreen, corners: [.topLeft, .topRight, .bottomLeft, .bottomRight])
            self.jellyAnimator = JellyAnimator(presentation:presentation)
            self.jellyAnimator?.prepare(viewController: navViewController)
            
            let viewController = navViewController.topViewController as! TextListViewController
            viewController.completionHandler = { controller in
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Enums
    private enum LivePhotoMode {
        case on
        case off
    }
    
    private enum DepthDataDeliveryMode {
        case on
        case off
    }
    
    
    // MARK: - Session Management
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    private let session = AVCaptureSession()
    
    private var isSessionRunning = false
    
    private let sessionQueue = DispatchQueue(label: "session queue",
                                             attributes: [],
                                             target: nil) // Communicate with the session and other session objects on this queue.
    
    private var setupResult: SessionSetupResult = .notAuthorized
    
    var videoDeviceInput: AVCaptureDeviceInput!
    @IBOutlet private weak var cameraUnavailableLabel: UILabel!
    @IBOutlet private weak var previewView: PreviewView!
    
    // Call this on the session queue.
    private func configureSession() {
        
        session.beginConfiguration()
        
        /*
         We do not create an AVCaptureMovieFileOutput when setting up the session because the
         AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
         */
        session.sessionPreset = AVCaptureSession.Preset.photo
        
        // Add video input.
        do {
            let defaultVideoDevice = AVCaptureDevice.default(for: AVMediaType.video)
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice!)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                self.setupResult = .success
                DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    if statusBarOrientation != .unknown {
                        if let videoOrientation = statusBarOrientation.videoOrientation {
                            initialVideoOrientation = videoOrientation
                        }
                    }
                    
                    self.previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                    self.previewView.videoPreviewLayer.connection?.videoOrientation = initialVideoOrientation
                }
            } else {
                print("Could not add video device input to the session")
                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        } catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add photo output.
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
            photoOutput.isDepthDataDeliveryEnabled = photoOutput.isDepthDataDeliverySupported
            
        } else {
            print("Could not add photo output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        session.commitConfiguration()
    }
    
    // MARK: - KVO and Notifications
    
    private var sessionRunningObserveContext = 0
    
    private func addObservers() {
        session.addObserver(self, forKeyPath: "running", options: .new, context: &sessionRunningObserveContext)
        
        NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: .AVCaptureSessionRuntimeError, object: session)
        
        /*
         A session can only run when the app is full screen. It will be interrupted
         in a multi-app layout, introduced in iOS 9, see also the documentation of
         AVCaptureSessionInterruptionReason. Add observers to handle these session
         interruptions and show a preview is paused message. See the documentation
         of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
         */
        NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: .AVCaptureSessionWasInterrupted, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: .AVCaptureSessionInterruptionEnded, object: session)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
        session.removeObserver(self, forKeyPath: "running", context: &sessionRunningObserveContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &sessionRunningObserveContext {
            let newValue = change?[.newKey] as AnyObject?
            guard let isSessionRunning = newValue?.boolValue else { return }
            DispatchQueue.main.async { [unowned self] in
                self.photoButton.isEnabled = isSessionRunning
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    @objc
    func sessionRuntimeError(notification: NSNotification) {
        guard let errorValue = notification.userInfo?[AVCaptureSessionErrorKey] as? NSError else {
            return
        }
        
        let error = AVError(_nsError: errorValue)
        print("Capture session runtime error: \(error)")
        
        /*
         Automatically try to restart the session running if media services were
         reset and the last start running succeeded. Otherwise, enable the user
         to try to resume the session running.
         */
        if error.code == .mediaServicesWereReset {
            sessionQueue.async { [unowned self] in
                if self.isSessionRunning {
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                } else {
                }
            }
        } else {
        }
    }
    
    @objc
    func sessionWasInterrupted(notification: NSNotification) {
        /*
         In some scenarios we want to enable the user to resume the session running.
         For example, if music playback is initiated via control center while
         using AVCam, then the user can let AVCam resume
         the session running, which will stop music playback. Note that stopping
         music playback in control center will not automatically resume the session
         running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
         */
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSession.InterruptionReason(rawValue: reasonIntegerValue) {
            print("Capture session was interrupted with reason \(reason)")
            
            if reason == .audioDeviceInUseByAnotherClient || reason == .videoDeviceInUseByAnotherClient {
                //                showResumeButton = true
            } else if reason == .videoDeviceNotAvailableWithMultipleForegroundApps {
                // Simply fade-in a label to inform the user that the camera is unavailable.
                cameraUnavailableLabel.alpha = 0
                cameraUnavailableLabel.isHidden = false
                UIView.animate(withDuration: 0.25) { [unowned self] in
                    self.cameraUnavailableLabel.alpha = 1
                }
            }
            
            
        }
    }
    
    @objc
    func sessionInterruptionEnded(notification: NSNotification) {
        print("Capture session interruption ended")
        
        if !cameraUnavailableLabel.isHidden {
            UIView.animate(withDuration: 0.25,
                           animations: { [unowned self] in
                            self.cameraUnavailableLabel.alpha = 0
                }, completion: { [unowned self] _ in
                    self.cameraUnavailableLabel.isHidden = true
                }
            )
        }
    }
    
    
    // MARK: - Capturing Photos
    
    private let photoOutput = AVCapturePhotoOutput()
    
    private var inProgressPhotoCaptureDelegates = [Int64: PhotoCaptureProcessor]()
    
    @IBOutlet private weak var photoButton: UIButton!
    @IBAction private func capturePhoto(_ photoButton: UIButton) {
        
        if setupResult != .success {
            let alertView = self.permissionAlert()
            alertView.showInfo("FYI", subTitle: "Smart Text needs camera permission to work correctly")
            return
        }
        
        self.shutterSoundPlayer?.play()
        
        /*
         Retrieve the video preview layer's video orientation on the main queue before
         entering the session queue. We do this to ensure UI elements are accessed on
         the main thread and session configuration is done on the session queue.
         */
        let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection?.videoOrientation
        
        sessionQueue.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            if let photoOutputConnection = self.photoOutput.connection(with: AVMediaType.video) {
                photoOutputConnection.videoOrientation = videoPreviewLayerOrientation!
            }
            
            var photoSettings = AVCapturePhotoSettings()
            // Capture HEIF photo when supported, with flash set to auto and high resolution photo enabled.
            if  self.photoOutput.availablePhotoCodecTypes.contains(AVVideoCodecType(rawValue: AVVideoCodecType.hevc.rawValue)) {
                
                photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.hevc])
                
            }
            
            if self.videoDeviceInput.device.isFlashAvailable {
                photoSettings.flashMode = .auto
            }
            
            photoSettings.isHighResolutionPhotoEnabled = true

            // Use a separate object for the photo capture delegate to isolate each capture life cycle.
            let photoCaptureProcessor = PhotoCaptureProcessor(with: photoSettings, willCapturePhotoAnimation: {
                DispatchQueue.main.async { [unowned self] in
                    self.previewView.videoPreviewLayer.opacity = 0
                    UIView.animate(withDuration: 0.25) { [unowned self] in
                        self.previewView.videoPreviewLayer.opacity = 1
                    }
                }
            }, livePhotoCaptureHandler: { capturing in
                // nothing needed in here
            }, completionHandler: { [unowned self] photoCaptureProcessor in
                if let image = photoCaptureProcessor.capturedImage {
                    self.prepareCaptured(image: image)
                } else {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false,
                        contentViewCornerRadius: CGFloat(15),
                        buttonCornerRadius: CGFloat(7),
                        hideWhenBackgroundViewIsTapped: true
                    )
                    
                    let alertView = SCLAlertView(appearance:appearance)
                    alertView.addButton("OK", action: {
                        alertView.hideView()
                    })
                    
                    alertView.showError("Error", subTitle: "An error occurred while trying to take a photo. Please try again.")
                }
                
                // When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
                self.sessionQueue.async { [unowned self] in
                    self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = nil
                }
                }
            )
            
            /*
             The Photo Output keeps a weak reference to the photo capture delegate so
             we store it in an array to maintain a strong reference to this object
             until the capture is completed.
             */
            self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = photoCaptureProcessor
            self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureProcessor)
            
            self.shutterSoundPlayer?.prepareToPlay()

        }
    }
    
    private func prepareCaptured(image: UIImage?) {
        
        let controller = CropViewController()
        controller.toolbarHidden = true
        controller.delegate = self
        controller.image = image
        
        let navController = UINavigationController(rootViewController: controller)
        navController.transitioningDelegate = self
        navController.isHeroEnabled = true
        navController.heroModalAnimationType = .zoom
        
        present(navController, animated: true, completion: nil)
    }
    
    // MARK: - Transition delegate
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = StarWarsGLAnimator()
        animator.duration = 1
        animator.spriteWidth = 15
        return animator
    }

    
    // MARK: - Pick image from library
    func openPhotoAlbum() {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
        
    }
    
    
    // MARK: - UIImagePickerController delegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        dismiss(animated: true) { [unowned self] in
            self.prepareCaptured(image: image)
        }
    }
    
    
    // MARK: - Cropping image delegate
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        //        controller.dismissViewControllerAnimated(true, completion: nil)
        //        imageView.image = image
        //        updateEditButtonEnabled()
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true) {
            self.togglePreviewImage(hidden: false, image: image)
            // show hud
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Converting...", presentingView: self.view)
            
            DispatchQueue.main.async {
                self.scanForText(image: image)
            }
        }
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - OCR
    
    func performImageRecognition(image: UIImage) {
        tesseract.image = image.g8_blackAndWhite();
        tesseract.recognize();
        
        guard let recognizedText = tesseract.recognizedText else {
            return
        }
        
        guard recognizedText.count > 0 else {
            self.togglePreviewImage(hidden: true, image: nil)
            return
        }
        
        APESuperHUD.removeHUD(animated: true, presentingView: self.view) {
            self.togglePreviewImage(hidden: true, image: nil)
            self.showScanResult(image: image, text: recognizedText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        }
    }
    
    func togglePreviewImage(hidden: Bool, image: UIImage?) {
//        if hidden {
//            self.previewImage.image = nil
//            self.previewImage.isHidden = true
//        } else {
//            self.previewImage.isHidden = false
//            self.previewImage.image = image//?.g8_grayScale()
//        }
    }
    
    // MARK: - OCR Delegate
    func progressImageRecognition(for tesseract: G8Tesseract!) {
//        let progress = Float(tesseract.progress)
//        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "\(progress)%", presentingView: self.view)
    }
    
    // MARK: - Scan text and show result
    func scanForText(image: UIImage) {
        self.performImageRecognition(image: image)
    }
    
    func showScanResult(image: UIImage, text: String) {
        let newRecord = self.textToRecord(textIn: text, image: image)
        
        let textVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TextViewController") as! RecordViewController
        textVC.aTextRecord = newRecord
        textVC.isEditeMode = true
        textVC.cancelCompletionHandler = { (optionalRecord: TextRecord?) -> Void in
            if let aRecord = optionalRecord {
                try! self.db.operation { (context, save) throws -> Void in
                    try! context.remove(aRecord)
                    save()
                }
            }
            self.navigationController?.popViewController(animated: true)
        }
        textVC.saveCompletionHandler = { (optionalRecord: TextRecord?) -> Void in
            self.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.pushViewController(textVC, animated: true)
    }
    
    func textToRecord(textIn: String, image: UIImage) -> TextRecord? {
        
        var aRecord: TextRecord?
        
        try! self.db.operation { (context, save) throws -> Void in
            aRecord = try! context.create()
            aRecord?.content = textIn
            aRecord?.type = StructuredDataType.plainText.rawValue
            aRecord?.image = UIImageJPEGRepresentation(image, 1.0)
            aRecord?.thumbNail = UIImageJPEGRepresentation(image, 0.3)
            aRecord?.date = Date()
            
            let types: NSTextCheckingResult.CheckingType = [.address, .phoneNumber, .link, .transitInformation]
            let detector = try? NSDataDetector(types: types.rawValue)
            if let tempText = aRecord?.content {
                detector?.enumerateMatches(in: tempText, range: NSMakeRange(0, tempText.utf16.count)) {
                    (result, _, _) in
                    
                    guard let result = result else {
                        // no structure text found
                        return
                    }
                    
                    if result.resultType == .link {
                        guard let urlString = result.url?.absoluteString else {
                            return
                        }
                        if let email = self.getEmailAddress(from: urlString).first {
                            let emailData: StructuredData = try! context.create()
                            emailData.content = email
                            emailData.type = StructuredDataType.email.rawValue
                            aRecord?.addToStructuredData(emailData)
                        } else {
                            let urlData: StructuredData = try! context.create()
                            urlData.content = urlString
                            urlData.type = StructuredDataType.website.rawValue
                            aRecord?.addToStructuredData(urlData)
                        }
                    } else if result.resultType == .address {
                        let addresses = self.getAddressData(from: textIn)
                        for addr in addresses {
                            if addr.values.count > 0 {
                                let address: Address = try! context.create()
                                address.type = StructuredDataType.address.rawValue
                                address.street = addr[NSTextCheckingKey.street] ?? ""
                                address.city = addr[NSTextCheckingKey.city] ?? ""
                                address.state = addr[NSTextCheckingKey.state] ?? ""
                                address.zipcode = addr[NSTextCheckingKey.zip] ?? ""
                                address.country = addr[NSTextCheckingKey.country] ?? ""
                                aRecord?.addToAddress(address)
                            }
                        } // TODO: need to handle scanning address better
                    } else if result.resultType == .phoneNumber {
                        let phoneData: StructuredData = try! context.create()
                        phoneData.content = result.phoneNumber
                        phoneData.type = StructuredDataType.phone.rawValue
                        aRecord?.addToStructuredData(phoneData)
                    }
                }
            }
            
            save()
        }
        
        return aRecord
    }
    
    // MARK: - Data Extraction Methods
    
    /// Extracts email addresses and returns an array of email address strings
    func getEmailAddress(from dataString: String) -> [String] {
        let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        // (4):
        let matches = regex.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.characters.count))
        
        var emailArray = [String]()
        
        for match in matches {
            let email = (dataString as NSString).substring(with: match.range)
            emailArray.append(email)
        }
        
        return emailArray
    }
    
    /// Extracts a phone number and returns an array of phone number strings
    func getPhoneNumber(from dataString: String) -> [String] {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: dataString, options: [], range: NSRange(location: 0, length: dataString.utf16.count))
        
        var phoneNumberArray = [String]()
        
        // put matches into String array
        for match in matches {
            let phoneNumber = (dataString as NSString).substring(with: match.range)
            phoneNumberArray.append(phoneNumber)
        }
        
        // remove non-numeric characters from phone number
        var index = 0 // starts the index of the array off at zero
        for var phoneNumber in phoneNumberArray {
            let cleanPhoneNumber = String(phoneNumber.characters.filter { String($0).rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil })
            // take out the dirty phone number
            phoneNumberArray.remove(at: index)
            // insert the clean phone number
            phoneNumberArray.insert(cleanPhoneNumber, at: index)
            // increment for the next loop through phoneNumberArray
            index += 1
        }
        
        return phoneNumberArray
    }
    
    /// Returns array of address dictionaries from a String
    func getAddressData(from string: String) -> [Dictionary<NSTextCheckingKey, String>] {
        var addressData = [Dictionary<NSTextCheckingKey, String>()]
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.address.rawValue)
        let matches = detector.matches(in: string, options: [], range: NSRange(location: 0, length: string.utf16.count))
        
        // put matches into String array
        for match in matches {
            if match.resultType == .address {
                addressData.append(match.addressComponents!)
            }
        }
        
        return addressData
    }
    
    // Returns array of Date objects contained in a String
    func getDateData(from string: String) -> [Date] {
        var dateData = [Date]()
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.date.rawValue)
        let matches = detector.matches(in: string, options: [], range: NSRange(location: 0, length: string.utf16.count))
        
        // put matches into String array
        for match in matches {
            if match.resultType == .date {
                dateData.append(match.date!)
            }
        }
        
        return dateData
    }
    
    // MARK: - Settings delegate
    
    func scanTextWithPhotoLibrary(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            self.openPhotoAlbum()
        }
    }
    
    func contactUs(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["bestofarkit@gmail.com"])
            composeVC.setSubject("Smarter Text")
            composeVC.setMessageBody("Let us know how we can help. \n\n Thank you for your feedbacks!", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func rateTheApp(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            
        }
    }
    
    func facebook(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            let url = URL(string: "https://facebook.com/bestofarkit")
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true)
        }
    }
    
    func twitter(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            let url = URL(string: "https://twitter.com/bestofarkit")
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true)
        }
    }
    
    func visitWebsite(_ controller: SettingsCollectionViewController) {
        controller.dismiss(animated: true) {
            let url = URL(string: "http://bestofarkit.com")
            let safariVC = SFSafariViewController(url: url!)
            safariVC.delegate = self
            self.present(safariVC, animated: true)
        }
    }
    
    // MARK: - Safari delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Mail delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        let alertController = UIAlertController(title: "title", message: "message", preferredStyle: .alert)
        let alertDismiss = UIAlertAction(title: "OK", style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(alertDismiss)
        
        // Check the result or perform other tasks.
        switch result {
        case .failed:
            alertController.title = "Error"
            alertController.message = "Unable to send mail. Please check your internet connection and also make sure you can send emails from your phone."
        case .sent:
            alertController.title = "Your message is on its way to us. Thank You!"
            alertController.message = ""
        case .saved:
            alertController.title = "Saved"
            alertController.message = "Your message has been saved. You can continue your draft by opening it through your mail app."
        case .cancelled:
            print("canceled mail")
//        default:
//            print("default")
        }
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true) {
            if result != MFMailComposeResult.cancelled {
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Permission delegate
    func permissionsViewControllerDidContinue(_ viewController: PAPermissionsViewController) {
        viewController.dismiss(animated: true) {
            let controller = viewController as! PermissionsViewController
            // start session if camera and audio is allowed
            let cameraStatus = controller.cameraCheck.status
            if cameraStatus == .enabled {
                self.setupResult = .success
                self.setupCameraFeed()
                self.sessionQueue.async {
                    self.addObservers()
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                }
                self.toggleCameraLabel(hidden: true)
            } else {
                let alertView = self.permissionAlert()
                alertView.showInfo("FYI", subTitle: "Smart Text needs camera permission to work correctly")
                
                self.toggleCameraLabel(hidden: false)
            }
        }
    }
    
}


// MARK: - Extensions

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeRight
        case .landscapeRight: return .landscapeLeft
        default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}

extension AVCaptureDevice.DiscoverySession {
    func uniqueDevicePositionsCount() -> Int {
        var uniqueDevicePositions: [AVCaptureDevice.Position] = []
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        
        return uniqueDevicePositions.count
    }
}

extension String {
    
    private func removeCharacters(unicodeScalarsFilter: (UnicodeScalar) -> Bool) -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{unicodeScalarsFilter($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    private func removeCharacters(from charSets: [CharacterSet], unicodeScalarsFilter: (CharacterSet, UnicodeScalar) -> Bool) -> String {
        return removeCharacters{ unicodeScalar in
            for charSet in charSets {
                let result = unicodeScalarsFilter(charSet, unicodeScalar)
                if result {
                    return true
                }
            }
            return false
        }
    }
    
    func removeCharacters(charSets: [CharacterSet]) -> String {
        return removeCharacters(from: charSets) { charSet, unicodeScalar in
            !charSet.contains(unicodeScalar)
        }
    }
    
    func removeCharacters(charSet: CharacterSet) -> String {
        return removeCharacters(charSets: [charSet])
    }
    
    func onlyCharacters(charSets: [CharacterSet]) -> String {
        return removeCharacters(from: charSets) { charSet, unicodeScalar in
            charSet.contains(unicodeScalar)
        }
    }
    
    func onlyCharacters(charSet: CharacterSet) -> String {
        return onlyCharacters(charSets: [charSet])
    }
    
    var onlyDigits: String {
        return onlyCharacters(charSets: [.decimalDigits])
    }
    
    var onlyLetters: String {
        return onlyCharacters(charSets: [.letters])
    }
}
