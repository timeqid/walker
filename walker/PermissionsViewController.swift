//
//  PermissionsViewController.swift
//  walker
//
//  Created by John Tran on 8/17/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import PAPermissions

class PermissionsViewController: PAPermissionsViewController {

    let cameraCheck = PACameraPermissionsCheck()
    let contactsCheck  : PAPermissionsCheck = {
        if #available(iOS 9.0, *) {
            return PACNContactsPermissionsCheck()
        } else {
            return PAABAddressBookCheck()
        }
    }()
    lazy var notificationsCheck : PAPermissionsCheck = {
        if #available(iOS 10.0, *) {
            return PAUNNotificationPermissionsCheck()
        } else {
            return PANotificationsPermissionsCheck()
        }
    }()
    let locationCheck = PALocationPermissionsCheck()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleText = "Smart Text"
        self.detailsText = "Please enable these permissions"
        self.tintColor = UIColor.red
        
        let permissions = [
            PAPermissionsItem.itemForType(.camera, reason: "Needed to capture images")!,
            PAPermissionsItem.itemForType(.contacts, reason: "Needed to create contact if you want to")!,
            PAPermissionsItem.itemForType(.notifications, reason: "Needed to send you important notifications (very seldomly)")!,
            PAPermissionsItem.itemForType(.location, reason: "Needed to map addresses for you")!]
        
        let handlers = [
            PAPermissionsType.location.rawValue: self.locationCheck,
            PAPermissionsType.camera.rawValue: self.cameraCheck,
            PAPermissionsType.contacts.rawValue:self.contactsCheck,
            PAPermissionsType.notifications.rawValue:self.notificationsCheck]
        self.setupData(permissions, handlers: handlers)
        
    }

//    func permissionCheck(_ permissionCheck: PAPermissionsCheck, didCheckStatus: PAPermissionsStatus) {
//        super.permissionCheck(permissionCheck, didCheckStatus: didCheckStatus)
//        
//    }
}
