//
//  TextViewController.swift
//  walker
//
//  Created by John Tran on 7/29/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import Vision
import TesseractOCR
import SugarRecord
import CoreData
import SCLAlertView
import BFRImageViewer
import FloatingActionSheetController
import MessageUI
import SafariServices
import AddressBookUI
import Contacts
import ContactsUI

class RecordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
                            MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate,
                            SFSafariViewControllerDelegate {
    
    /*
     ----- iCloud
     // Initializes the CoreDataiCloudStorage
     func icloudStorage() -> CoreDataiCloudStorage {
     let bundle = Bundle(forClass: self.classForCoder())
     let model = CoreData.ObjectModel.merged([bundle])
     let icloudConfig = CoreDataiCloudConfig(ubiquitousContentName: "MyDb", ubiquitousContentURL: "Path/", ubiquitousContainerIdentifier: "com.company.MyApp.anothercontainer")
     return CoreDataiCloudStorage(model: model, iCloud: icloudConfig)
     }
     */
    lazy var db: CoreDataDefaultStorage = {
        let store = CoreDataStore.named("walker")
        let bundle = Bundle(for: RecordViewController.classForCoder())
        let model = CoreDataObjectModel.merged([bundle])
        let defaultStorage = try! CoreDataDefaultStorage(store: store, model: model)
        return defaultStorage
    }()
    
    var alertViewAppearance: SCLAlertView.SCLAppearance!
    
    @IBOutlet weak var tableView: UITableView!
    var aTextRecord: TextRecord?
    
    var saveBtn: UIBarButtonItem?
    var createContactBtn: UIBarButtonItem?
    
    var isEditeMode: Bool = false
    
    var structuredDataList: [StructuredData] = []
    var addressDataList: [Address] = []
    
    var cancelCompletionHandler: ((TextRecord?)->Void)?
    var saveCompletionHandler: ((TextRecord?)->Void)?
    
    lazy var recordObservable: RequestObservable<TextRecord> = {
        let request = FetchRequest<TextRecord>().sorted(with: "date", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var structuredDataObservable: RequestObservable<StructuredData> = {
        let request = FetchRequest<StructuredData>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var plainTextObservable: RequestObservable<TextRecord> = {
        let request = FetchRequest<TextRecord>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var addressObservable: RequestObservable<Address> = {
        let request = FetchRequest<Address>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.structuredDataList = aTextRecord?.structuredData?.allObjects as! [StructuredData]
        self.structuredDataList = self.structuredDataList.sorted { (object1, object2) -> Bool in
            return object1.type! < object2.type!
        }
        self.addressDataList = aTextRecord?.address?.allObjects as! [Address]
        
        if self.isEditeMode {
            saveBtn = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(save))
            self.navigationItem.rightBarButtonItem = saveBtn
        } else if self.structuredDataList.count > 0 || self.addressDataList.count > 0 {
            createContactBtn = UIBarButtonItem(title: "Create Contact", style: .plain, target: self, action: #selector(createNewContactTapped))
            self.navigationItem.rightBarButtonItem = createContactBtn
        }
        
        let closeBtn = UIBarButtonItem(title: "Back", style: .plain , target: self, action: #selector(close(tempRecord:)))
        self.navigationItem.leftBarButtonItem = closeBtn
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        alertViewAppearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            contentViewCornerRadius: CGFloat(15),
            buttonCornerRadius: CGFloat(7),
            hideWhenBackgroundViewIsTapped: true
        )
        
        self.setupObservable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func setupObservable() {
        structuredDataObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load detail, do nothing")
            case .update:
                print("data was delete or updated")
//                DispatchQueue.main.async {
//                    self.refreshData()
//                    DispatchQueue.main.async {
//                        self.tableView.reloadData()
//                    }
//                }
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        plainTextObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load detail, do nothing")
            case .update:
                print("delete or updated plain text")
//                self.tableView.reloadData()
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        addressObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load detail, do nothing")
            case .update:
                print("deleted or updated address ")
//                DispatchQueue.main.async {
//                    self.refreshData()
//                    DispatchQueue.main.async {
//                        self.tableView.reloadData()
//                    }
//                }
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
    }
    
    func refreshData() {
        // fetch new data
        if let oldContent = self.aTextRecord?.content {
            try! self.db.operation { (context, save) throws -> Void in
                self.aTextRecord = try context.request(TextRecord.self).filtered(with: "content", equalTo: oldContent).fetch().first
                self.structuredDataList = self.aTextRecord?.structuredData?.allObjects as! [StructuredData]
                self.structuredDataList = self.structuredDataList.sorted { (object1, object2) -> Bool in
                    return object1.type! < object2.type!
                }
                self.addressDataList = self.aTextRecord?.address?.allObjects as! [Address]
            }
        }
    }
    
    @objc
    func save() {
        guard let content = self.aTextRecord?.content else { return }
        
        if content.isEmpty {
            let alertView = SCLAlertView(appearance:alertViewAppearance)
            alertView.addButton("Save Anyway") {
                if let callBack = self.saveCompletionHandler {
                    callBack(self.aTextRecord)
                }
            }
            alertView.addButton("Back", action: {
                alertView.hideView()
            })
            
            alertView.showInfo("No Text Found", subTitle: "There is no text content. Do you still want to save it?")
        } else {
            if let callBack = self.saveCompletionHandler {
                callBack(self.aTextRecord)
            }
        }
    }
    
    @objc
    func close(tempRecord: TextRecord) {
        try! self.db.operation { (context, save) throws -> Void in
            save()
        }
        if let callBack = self.cancelCompletionHandler {
            callBack(self.aTextRecord!)
        }
    }
    
    @objc func createNewContactTapped() {
        if CNContactStore.authorizationStatus(for: .contacts) == .authorized {
            self.createNewContact()
        } else {
            DispatchQueue.main.async {
                let store = CNContactStore()
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false,
                    contentViewCornerRadius: CGFloat(15),
                    buttonCornerRadius: CGFloat(7),
                    hideWhenBackgroundViewIsTapped: true
                )
                
                store.requestAccess(for: .contacts, completionHandler: { (allowed, error) in
                    if allowed {
                        self.createNewContact()
                    } else {
                        DispatchQueue.main.async {
                        let denyAlert = SCLAlertView(appearance:appearance)
                        denyAlert.addButton("Settings", action: {
                            denyAlert.hideView()
                            let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
                            UIApplication.shared.open(settingsURL!, options: [:], completionHandler: nil)
                        })
                        denyAlert.addButton("Cancel", action: {
                            denyAlert.hideView()
                        })
                        denyAlert.showError("Permission Denied", subTitle: "Smarter Text was not allowed to access contacts. Please allow access in Settings.")
                        }
                    }
                })
            }
        }
    }
    
    func createNewContact() {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            contentViewCornerRadius: CGFloat(15),
            buttonCornerRadius: CGFloat(7),
            hideWhenBackgroundViewIsTapped: true
        )
        
        let createAlertView = SCLAlertView(appearance:appearance)
        // This will create a new contact and save it to \"Contacts\" on your phone. You can edit or delete it from there.
        let fNameField = createAlertView.addTextField("First Name")
        let lNameField = createAlertView.addTextField("Last Name")
        
        createAlertView.addButton("Create", action: {
            guard let fName = fNameField.text, let lName = lNameField.text else {
                return
            }
            
            if fName.isEmpty && lName.isEmpty {
                DispatchQueue.main.async {
                    self.alert().showError("Empty Fields", subTitle: "Contact must have at least a first name or last name.")
                }
                return
            }
            
            let store = CNContactStore()
            
            let contactToAdd = CNMutableContact()
            contactToAdd.givenName = fName
            contactToAdd.familyName = lName
            var urlArr = [CNLabeledValue<NSString>]()
            var emailArr = [CNLabeledValue<NSString>]()
            var phoneArr = [CNLabeledValue<CNPhoneNumber>]()
            var addressArr = [CNLabeledValue<CNPostalAddress>]()
            
            for item in self.structuredDataList {
                switch item.type! {
                case StructuredDataType.email.rawValue:
                    if let email = item.content {
                        let email = CNLabeledValue(label: CNLabelWork, value: email as NSString)
                        emailArr.append(email)
                    }
                case StructuredDataType.website.rawValue:
                    if let url = item.content {
                        let urlLabel = CNLabeledValue(label: CNLabelURLAddressHomePage, value: url as NSString)
                        urlArr.append(urlLabel)
                    }
                case StructuredDataType.phone.rawValue:
                    if let number = item.content {
                        let mobileNumber = CNPhoneNumber(stringValue: number)
                        let mobileValue = CNLabeledValue(label: CNLabelPhoneNumberMobile, value: mobileNumber)
                        phoneArr.append(mobileValue)
                    }
                default:
                    print("blah blah")
                }
            }
            
            for addr in self.addressDataList {
                let address = CNMutablePostalAddress()
                if let street = addr.street {
                    address.street = street
                }
                if let city = addr.city {
                    address.city = city
                }
                if let state = addr.state {
                    address.state = state
                }
                if let zip = addr.zipcode {
                    address.postalCode = zip
                }
                if let country = addr.country {
                    address.country = country
                }
                let workAddress = CNLabeledValue<CNPostalAddress>(label:CNLabelWork, value:address)
                addressArr.append(workAddress)
            }
            
            if emailArr.count > 0 {
                contactToAdd.emailAddresses = emailArr
            }
            
            if urlArr.count > 0 {
                // supposed to be able to add more than one, but iOS can't handle that now
                contactToAdd.urlAddresses = [urlArr.first!]
            }
            
            if phoneArr.count > 0 {
                contactToAdd.phoneNumbers = phoneArr
            }
            
            if addressArr.count > 0 {
                contactToAdd.postalAddresses = addressArr
            }
            
            if let imageData = self.aTextRecord?.image {
                contactToAdd.imageData = imageData
            }
            
            let saveRequest = CNSaveRequest()
            saveRequest.add(contactToAdd, toContainerWithIdentifier: nil)
            
            do {
                try store.execute(saveRequest)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.alert().showSuccess("Contact Created", subTitle: "A new contact was created successfully.")
                })
            } catch {
                print("Error adding new contact \(error)")
                DispatchQueue.main.async {
                    self.alert().showError("Error Creating Contact", subTitle: "Failed to create a new contact. Please try again later.")
                }
            }
        })
        
        createAlertView.addButton("Cancel", action: {
            createAlertView.hideView()
        })
        
        DispatchQueue.main.async {
            createAlertView.showInfo("Create new contact", subTitle: "Please fill in First Name and Last Name")
        }
    }
    
    func alert() -> SCLAlertView
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            contentViewCornerRadius: CGFloat(15),
            buttonCornerRadius: CGFloat(7),
            hideWhenBackgroundViewIsTapped: true
        )
        
        let alertView = SCLAlertView(appearance:appearance)
        alertView.addButton("OK", action: {
            alertView.hideView()
        })
        
        return alertView
    }
    
    func NSTextCheckingTypesFromUIDataDetectorTypes(dataDetectorType: UIDataDetectorTypes) -> NSTextCheckingResult.CheckingType {
        var textCheckingType: NSTextCheckingResult.CheckingType = []
        
        if dataDetectorType.contains(.address) {
            textCheckingType.insert(.address)
        }
        
        if dataDetectorType.contains(.calendarEvent) {
            textCheckingType.insert(.date)
        }
        
        if dataDetectorType.contains(.link) {
            textCheckingType.insert(.link)
        }
        
        if dataDetectorType.contains(.phoneNumber) {
            textCheckingType.insert(.phoneNumber)
        }
        
        return textCheckingType
    }
    
    
    // MARK: Table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var secNum = 2
        
        if self.structuredDataList.count > 0 {
            secNum += 1
        }
        
        if self.addressDataList.count > 0 {
            secNum += 1
        }
        
        return secNum
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 && self.structuredDataList.count > 0 {
            return self.structuredDataList.count
        } else if section == 2 && self.addressDataList.count > 0 {
            return self.addressDataList.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SouceImageCell", for: indexPath) as! SouceImageCell
            return self.configure(imageCell: cell, indexPath: indexPath)
        } else {
            if self.structuredDataList.count > 0 || self.addressDataList.count > 0 {
                if self.structuredDataList.count > 0 && indexPath.section == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "StructuredDataCell", for: indexPath) as! StructuredDataCell
                    cell.structuredRecord = self.structuredDataList[indexPath.item]
                    return self.configureStructuredData(cell: cell, indexPath: indexPath)
                } else if (self.addressDataList.count > 0 && self.structuredDataList.count > 0 && indexPath.section == 2) ||
                    (self.structuredDataList.count == 0 && indexPath.section == 1)  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddressDataCell", for: indexPath) as! AddressDataCell
                    cell.addressRecord = self.addressDataList[indexPath.item]
                    return self.configureAddress(cell: cell, indexPath: indexPath)
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PlainTextCell", for: indexPath) as! PlainTextCell
                    cell.aRecord = self.aTextRecord
                    cell.textLabel?.text = self.aTextRecord?.content
                    cell.textLabel?.numberOfLines = 0
                    return cell
                }
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlainTextCell", for: indexPath) as! PlainTextCell
                cell.aRecord = self.aTextRecord
                cell.textLabel?.text = self.aTextRecord?.content
                cell.textLabel?.numberOfLines = 0
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let cell = tableView.cellForRow(at: indexPath)
        if cell is StructuredDataCell || cell is AddressDataCell {
            return true
        } else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let cell = tableView.cellForRow(at: indexPath)
            if cell is StructuredDataCell {
                let data = structuredDataList[indexPath.item]
                do {
                    try db.operation { (context, save) throws -> Void in
                        let item = try context.request(StructuredData.self).filtered(with: "content", equalTo: data.content ?? "").fetch().first
                        if let item = item {
                            self.structuredDataList.remove(at: indexPath.item)
                            try! context.remove(item)
                            if self.structuredDataList.count == 0 {
                                tableView.deleteSections(IndexSet(arrayLiteral: indexPath.section), with: .automatic)
                            } else {
                                tableView.deleteRows(at: [indexPath], with: .automatic)
                            }
                            save()
                        }
                    }
                } catch {
                    print("Error deleting structured data")
                }
            } else if cell is AddressDataCell {
                let data = addressDataList[indexPath.item]
                do {
                    try db.operation { (context, save) throws -> Void in
                        let item = try context.request(Address.self).filtered(with: self.predicate(for: data)).fetch().first
                        if let item = item {
                            self.addressDataList.remove(at: indexPath.item)
                            try! context.remove(item)
                            if self.addressDataList.count == 0 {
                                tableView.deleteSections(IndexSet(arrayLiteral: indexPath.section), with: .automatic)
                            } else {
                                tableView.deleteRows(at: [indexPath], with: .automatic)
                            }
                            save()
                        }
                    }
                } catch {
                    print("Error deleting address data")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let imageData = self.aTextRecord?.image {
                let image = UIImage(data: imageData)
                if let imageVC = BFRImageViewController(imageSource: [image!]) {
                    let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
                    imageVC.cc_setZoomTransition(originalView: cell.contentView)
                    self.present(imageVC, animated: true, completion: {
                        self.tableView.deselectRow(at: indexPath, animated: true)
                    })
                }
            }
        } else {
            let cell = tableView.cellForRow(at: indexPath)
            if cell is StructuredDataCell {
                guard let cellData = (cell as! StructuredDataCell).structuredRecord else { return }
                switch cellData.type! {
                case StructuredDataType.email.rawValue:
                    self.showEmailOption(cellData)
                case StructuredDataType.website.rawValue:
                    self.showLinkOption(cellData)
                case StructuredDataType.phone.rawValue:
                    self.showPhoneOption(cellData)
                default:
                    print("blah blah")
                }
            } else if cell is AddressDataCell {
                guard let cellData = (cell as! AddressDataCell).addressRecord else { return }
                self.showAddressOption(cellData)
            } else if cell is PlainTextCell {
                guard let data = self.aTextRecord?.content else { return }
                self.editPlainTextData(data)
            }
        }
    }
    
    func configure(imageCell: UITableViewCell, indexPath: IndexPath) -> UITableViewCell {
        let imgCell = imageCell as! SouceImageCell
        let image = UIImage(data: (self.aTextRecord?.thumbNail)!)
        imgCell.sourceImageView?.image = image
        return imageCell
    }
    
    func configureStructuredData(cell: UITableViewCell, indexPath: IndexPath) -> UITableViewCell {
        let result = self.structuredDataList[indexPath.item]
        switch result.type! {
        case StructuredDataType.email.rawValue:
            cell.textLabel?.text = (result.content ?? "")
            cell.imageView?.image = UIImage(named: "emailSmall")
            cell.tintColor = UIColor.jtEmail()
        case StructuredDataType.website.rawValue:
            cell.textLabel?.text = (result.content ?? "")
            cell.imageView?.image = UIImage(named: "linkSmall")
            cell.tintColor = UIColor.jtLink()
        case StructuredDataType.phone.rawValue:
            cell.textLabel?.text = (result.content ?? "")
            cell.imageView?.image = UIImage(named: "phone")
            cell.tintColor = UIColor.jtPhone()
        default:
            print("blah blah")
        }
        
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    func configureAddress(cell: UITableViewCell, indexPath: IndexPath) -> UITableViewCell {
        let result  = self.addressDataList[indexPath.item]
        var fullAddress = ""
        
        if let street = result.street {
            if street.count > 0 {
                fullAddress += street
            }
        }
        
        if let city = result.city {
            if city.count > 0 {
                fullAddress += "\n" + city
            }
        }
        
        if let state = result.state {
            if state.count > 0 {
                fullAddress += ", " + state
            }
        }
        
        if let zip = result.zipcode {
            if zip.count > 0 {
                fullAddress += " " + zip
            }
        }
        
        if let country = result.country {
            if country.count > 0 {
                fullAddress += "\n" + country
            }
        }
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = fullAddress
        cell.imageView?.image = UIImage(named: "map")
        cell.tintColor = UIColor.jtMap()
        
        return cell
    }
    
    // MARK: action options on data
    
    func showEmailOption(_ emailData: StructuredData?) {
        // TODO: Check if ft is available ?
        
        let sendEmail = FloatingAction(title: "Send An Email") { action in
            if let email = emailData?.content {
                self.send(email)
            }
        }
        
        let copyEmail = FloatingAction(title: "Copy Email") { action in
            if let email = emailData?.content {
                self.copyData(email)
            }
        }
        
        let editEmail = FloatingAction(title: "Edit") { action in
            if let email = emailData {
                self.editData(email)
            }
        }
        
        let cancel = FloatingAction(title: "Cancel") { action in
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
        let group1 = FloatingActionGroup(action: sendEmail, copyEmail, editEmail)
        let group2 = FloatingActionGroup(action: cancel)
        let optionsheet = FloatingActionSheetController(actionGroup: group1, group2)
        optionsheet.animationStyle = .pop
        optionsheet.itemTintColor = .white
        optionsheet.pushBackScale = 1.0
        optionsheet.textColor = UIColor.jtEmail()
        optionsheet.font = .boldSystemFont(ofSize: 18)
        optionsheet.present(in: self) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func showPhoneOption(_ phoneData: StructuredData?) {
        /*
         - See if it's ft available or viber or other callable app?
         - Add to existing contact ?
         - Create new contact ?
         */
        let call = FloatingAction(title: "Call") { action in
            if let phone = phoneData?.content {
                self.call(phone)
            }
        }
        
        let txt = FloatingAction(title: "Text") { action in
            if let phone = phoneData?.content {
                self.text(phone)
            }
        }
        
        let copy = FloatingAction(title: "Copy Number") { action in
            if let phone = phoneData?.content {
                self.copyData(phone)
            }
        }
        
        let editPhone = FloatingAction(title: "Edit") { action in
            if let phone = phoneData {
                self.editData(phone)
            }
        }
        
        let cancel = FloatingAction(title: "Cancel") { action in
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
        let group1 = FloatingActionGroup(action: call, txt, copy, editPhone)
        let group2 = FloatingActionGroup(action: cancel)
        let optionsheet = FloatingActionSheetController(actionGroup: group1, group2)
        optionsheet.animationStyle = .pop
        optionsheet.itemTintColor = .white
        optionsheet.pushBackScale = 1.0
        optionsheet.textColor = UIColor.jtPhone()
        optionsheet.font = .boldSystemFont(ofSize: 18)
        optionsheet.present(in: self) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func showLinkOption(_ linkData: StructuredData?) {
        
        let openInSafari = FloatingAction(title: "Open") { action in
            if let link = linkData?.content {
                self.openInSafari(link)
            }
        }
        
        let copy = FloatingAction(title: "Copy Link") { action in
            if let link = linkData?.content {
                self.copyData(link)
            }
        }
        
        let edit = FloatingAction(title: "Edit") { action in
            if let link = linkData {
                self.editData(link)
            }
        }
        
        let cancel = FloatingAction(title: "Cancel") { action in
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
        let group1 = FloatingActionGroup(action: openInSafari, copy, edit)
        let group2 = FloatingActionGroup(action: cancel)
        let optionsheet = FloatingActionSheetController(actionGroup: group1, group2)
        optionsheet.animationStyle = .pop
        optionsheet.itemTintColor = .white
        optionsheet.pushBackScale = 1.0
        optionsheet.textColor = UIColor.jtLink()
        optionsheet.font = .boldSystemFont(ofSize: 18)
        optionsheet.present(in: self) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    func showAddressOption(_ address: Address?) {
        /*
         - Add to existing contact ?
         - Create new contact ?
         */
        
        let openInMap = FloatingAction(title: "Open In Map") { action in
            if let address = address {
                self.openInMap(address)
            }
        }
        
        let copy = FloatingAction(title: "Copy Address") { action in
            if let address = address {
                self.copyAddress(address)
            }
        }
        
        let edit = FloatingAction(title: "Edit") { action in
            if let address = address {
                self.editAddressData(address)
            }
        }
        
        let cancel = FloatingAction(title: "Cancel") { action in
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
        let group1 = FloatingActionGroup(action: openInMap, copy, edit)
        let group2 = FloatingActionGroup(action: cancel)
        let optionsheet = FloatingActionSheetController(actionGroup: group1, group2)
        optionsheet.animationStyle = .pop
        optionsheet.itemTintColor = .white
        optionsheet.pushBackScale = 1.0
        optionsheet.textColor = UIColor.jtMap()
        optionsheet.font = .boldSystemFont(ofSize: 18)
        optionsheet.present(in: self) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    // MARK: - user actions
    func send(_ email: String) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([email])
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func text(_ phone: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.recipients = [phone.onlyDigits]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            let alertView = SCLAlertView(appearance:alertViewAppearance)
            alertView.addButton("OK", action: {
                alertView.hideView()
            })
            
            alertView.showError("Error", subTitle: "Please make sure you have cellular connection and the phone number is valid.")
        }
    }
    
    func call(_ phone: String) {
        if let phoneUrl = URL(string: "tel:" + phone.onlyDigits) {
            if UIApplication.shared.canOpenURL(phoneUrl) {
                UIApplication.shared.open(phoneUrl, completionHandler: { (done) in
                    print("finished phone call")
                })
            } else {
                let alertView = SCLAlertView(appearance:alertViewAppearance)
                alertView.addButton("OK", action: {
                    alertView.hideView()
                })
                
                alertView.showError("Error", subTitle: "Please make sure you have cellular connection and the phone number is valid.")
            }
        }
    }
    
    func copyData(_ data: String) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = data
        
        let alertView = SCLAlertView(appearance:alertViewAppearance)
        alertView.showSuccess("Copied", subTitle: "\(data)", duration:2.0, colorStyle: 0x22B573, colorTextButton: 0xFFFFFF, animationStyle: SCLAnimationStyle.noAnimation)
    }
    
    func editData(_ data: StructuredData) {
        let editNav = self.storyboard?.instantiateViewController(withIdentifier: "TextEditViewController") as! UINavigationController
        
        let textEdit = editNav.topViewController as! TextEditViewController
        textEdit.editedText = data.content
        textEdit.cancelCompletionHandler = {(controller) in
            controller.dismiss(animated: true, completion: {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
            })
        }
        
        textEdit.editCompletionHandler = { (editedText, controller) in
            if let newText = editedText {
                try! self.db.operation { (context, save) throws -> Void in
                    if let item = try! context.request(StructuredData.self)
                        .filtered(with: "content", equalTo: data.content ?? "").fetch().first {
                        item.content = newText
                        data.content = newText
                        save()
                    }
                }
            }
            
            controller.dismiss(animated: true, completion: nil)
        }
        
        editNav.cc_setZoomTransition(originalView: self.view)
        present(editNav, animated: true, completion: nil)
    }
    
    func editAddressData(_ data: Address) {
        let addressEdit = AddressEditViewController()
        addressEdit.address = data
        
        addressEdit.cancelCompletionHandler = {(controller) in
            controller.dismiss(animated: true, completion: {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
            })
        }
        
        // grab the original data before the it's being modified
        let stringPred = self.predicate(for: data)
        
        addressEdit.editCompletionHandler = { (editedAddress, controller) in
            if let address = editedAddress {
                try! self.db.operation { (context, save) throws -> Void in
                    if let each = try! context.request(Address.self)
                        .filtered(with: stringPred)
                        .fetch().first {
                        each.street = address.street
                        each.city = address.city
                        each.state = address.state
                        each.zipcode = address.zipcode
                        each.country = address.country
                        // save local
                        data.street = address.street
                        data.city = address.city
                        data.state = address.state
                        data.zipcode = address.zipcode
                        data.country = address.country
                        save()
                    }
                }
            }
            
            controller.dismiss(animated: true, completion: nil)
        }
        
        let editNav = UINavigationController(rootViewController: addressEdit)
        editNav.cc_setZoomTransition(originalView: self.view)
        present(editNav, animated: true, completion: nil)
    }
    
    func editPlainTextData(_ data: String) {
        let textEditNav = self.storyboard?.instantiateViewController(withIdentifier: "TextEditViewController") as! UINavigationController
        textEditNav.cc_setZoomTransition(originalView: self.view)
        let textEdit = textEditNav.topViewController as! TextEditViewController
        textEdit.editedText = data
        textEdit.cancelCompletionHandler = {(controller) in
            controller.dismiss(animated: true, completion: {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
            })
        }
        textEdit.editCompletionHandler = { (editedText, controller) in
            
            if let newText = editedText {
                try! self.db.operation { (context, save) throws -> Void in
                    if let item = try! context.request(TextRecord.self)
                        .filtered(with: "content", equalTo: data).fetch().first {
                        item.content = newText
                        self.aTextRecord?.content = newText
                        save()
                    }
                }
            }
            
            controller.dismiss(animated: true, completion: nil)
        }
        
        present(textEditNav, animated: true, completion: nil)
    }
    
    func openInSafari(_ link: String) {
        if let url = URL(string: link) {
            let safariVC = SFSafariViewController(url: url)
            safariVC.delegate = self
            present(safariVC, animated: true)
        } else {
            let alertView = SCLAlertView(appearance:alertViewAppearance)
            alertView.addButton("OK", action: {
                alertView.hideView()
            })
            
            alertView.showError("Error", subTitle: "Unable to open link. Please make sure the link is valid.")
        }
    }
    
    func openInMap(_ address: Address) {
        guard let addrString = self.addressToString(address) else {
            print("Error converting address to string")
            return
        }
        
        let mapUrl = "http://maps.apple.com/?address="
        
        if let addressUrl = URL(string: mapUrl + addrString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) {
            if UIApplication.shared.canOpenURL(addressUrl) {
                UIApplication.shared.open(addressUrl, completionHandler: { (done) in
                    print("finished map open")
                })
            } else {
                let alertView = SCLAlertView(appearance:alertViewAppearance)
                alertView.addButton("OK", action: {
                    alertView.hideView()
                })
                
                alertView.showError("Error", subTitle: "Unable to open this address.")
            }
        } else {
            let alertView = SCLAlertView(appearance:alertViewAppearance)
            alertView.addButton("OK", action: {
                alertView.hideView()
            })
            
            alertView.showError("Error", subTitle: "Unable to open this address. Please make sure it's a valid address.")
        }
    }
    
    func copyAddress(_ address: Address) {
        let alertView = SCLAlertView(appearance:alertViewAppearance)
        
        guard let addr = self.addressToString(address) else {
            alertView.showError("Error copying address.", subTitle: "", duration:2.0, colorStyle: 0xC1272D, colorTextButton: 0xFFFFFF, animationStyle: SCLAnimationStyle.noAnimation)
            return
        }
        
        let pasteboard = UIPasteboard.general
        pasteboard.string = addr
        
        alertView.showSuccess("Copied", subTitle: "\(addr)", duration:2.0, colorStyle: 0x22B573, colorTextButton: 0xFFFFFF, animationStyle: SCLAnimationStyle.noAnimation)
    }
    
    func addressToString(_ address: Address) -> String? {
        var addr = ""
        
        if let street = address.street {
            addr = street.count > 0 ? street : addr
        }
        
        if let city = address.city {
            addr = city.count > 0 ? addr + ", " + city : addr
        }
        
        if let state = address.state {
            addr = state.count > 0 ? addr + ", " + state + " " : addr
        }
        
        if let zip = address.zipcode {
            addr = zip.count > 0 ? addr + zip : addr
        }
        
        if let country = address.country {
            addr = country.count > 0 ? addr + ", " + country : addr
        }
        
        return addr
    }
    
    func predicate(for address: Address) -> NSPredicate {
        // grab the original data before the it's being modified
        let street = address.street ?? ""
        let city = address.city ?? ""
        let state = address.state ?? ""
        let zip = address.zipcode ?? ""
        let country = address.country ?? ""
        let stringPred = NSPredicate.init(format: "street == %@ AND city == %@ AND state == %@ AND zipcode == %@ AND country == %@",
                                          argumentArray: [street, city, state, zip, country])
        return stringPred
    }
    
    // MARK: - Mail delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        let alertController = UIAlertController(title: "title", message: "message", preferredStyle: .alert)
        let alertDismiss = UIAlertAction(title: "OK", style: .cancel) { (action) in
            self.dismiss(animated: true) {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
            }
        }
        alertController.addAction(alertDismiss)
        
        // Check the result or perform other tasks.
        switch result {
        case .failed:
            alertController.title = "Error"
            alertController.message = "Unable to send mail. Please check your internet connection and also make sure you can send emails from your phone."
        case .sent:
            print("sent")
        case .saved:
            alertController.title = "Saved"
            alertController.message = "Your message has been saved. You can continue your draft by opening it through your mail app."
        case .cancelled:
            print("canceled mail")
        }
        
        controller.dismiss(animated: true) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
            
            if result == MFMailComposeResult.failed || result == MFMailComposeResult.saved {
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - SMS delegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.view.endEditing(true)
        
        controller.dismiss(animated: true) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    // MARK: - Safari delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        //dismiss(animated: true)
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}

