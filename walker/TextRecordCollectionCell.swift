//
//  TextCell.swift
//  walker
//
//  Created by John Tran on 8/2/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit

class TextRecordCollectionCell: UICollectionViewCell {
    
    var textRecord: TextRecord?
    
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var capturedText: UITextView!
    
}
