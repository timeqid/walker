//
//  AddressEditViewController.swift
//  walker
//
//  Created by John Tran on 8/15/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import SwiftyFORM

class AddressEditViewController: FormViewController {

    var address: Address?
    var editCompletionHandler: ((Address?, AddressEditViewController)->Void)?
    var cancelCompletionHandler: ((AddressEditViewController)->Void)?
    
    var saveButton: UIBarButtonItem!
    var cancel: UIBarButtonItem!
    
    lazy var street: TextFieldFormItem = {
       let instance = TextFieldFormItem()
        instance.title = "Street"
        instance.value = self.address?.street ?? ""
        instance.placeholder = "Street Number & Name"
        instance.keyboardType = .default
        instance.autocorrectionType = .no
        instance.autocapitalizationType = .words
        instance.textDidChangeBlock = { value in
            self.address?.street = value
        }
        return instance
    }()
    
    lazy var city: TextFieldFormItem = {
        let instance = TextFieldFormItem()
        instance.title = "City"
        instance.value = self.address?.city ?? ""
        instance.placeholder = "City Name"
        instance.keyboardType = .default
        instance.autocorrectionType = .no
        instance.autocapitalizationType = .words
        instance.textDidChangeBlock = { value in
            self.address?.city = value
        }
        return instance
    }()
    
    lazy var state: TextFieldFormItem = {
        let instance = TextFieldFormItem()
        instance.title = "State"
        instance.value = self.address?.state ?? ""
        instance.placeholder = "State Name"
        instance.keyboardType = .default
        instance.autocorrectionType = .no
        instance.autocapitalizationType = .words
        instance.textDidChangeBlock = { value in
            self.address?.state = value
        }
        return instance
    }()
    
    lazy var zipcode: TextFieldFormItem = {
        let instance = TextFieldFormItem()
        instance.title = "Zip Code"
        instance.value = self.address?.zipcode ?? ""
        instance.placeholder = "Zip Code"
        instance.keyboardType = .numberPad
        instance.autocorrectionType = .no
        instance.textDidChangeBlock = { value in
            self.address?.zipcode = value
        }
        return instance
    }()
    
    lazy var country: TextFieldFormItem = {
        let instance = TextFieldFormItem()
        instance.title = "Country"
        instance.value = self.address?.country ?? ""
        instance.placeholder = "Country Name"
        instance.keyboardType = .default
        instance.autocorrectionType = .no
        instance.autocapitalizationType = .words
        instance.textDidChangeBlock = { value in
            self.address?.country = value
        }
        return instance
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Address"
        
        saveButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(saveAction(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        
        cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelAction(_:)))
        self.navigationItem.leftBarButtonItem = cancel
        
    }
    
    override func populate(_ builder: FormBuilder) {
        builder += street
        builder += city
        builder += state
        builder += zipcode
        builder += country
    }

    @objc func saveAction(_ sender: Any) {
        if let callBack = self.editCompletionHandler {
            callBack(self.address, self)
        }
    }
    
    @objc func cancelAction(_ sender: Any) {
        if let callBack = self.cancelCompletionHandler {
            callBack(self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
