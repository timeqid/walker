//
//  TextListViewController.swift
//  walker
//
//  Created by John Tran on 8/15/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit
import CoreData
import SugarRecord

private let reuseIdentifier = "TextListCell"

class TextListViewController: UITableViewController {

    lazy var db: CoreDataDefaultStorage = {
        let store = CoreDataStore.named("walker")
        let bundle = Bundle(for: RecordCollectionViewController.classForCoder())
        let model = CoreDataObjectModel.merged([bundle])
        let defaultStorage = try! CoreDataDefaultStorage(store: store, model: model)
        return defaultStorage
    }()
    
    lazy var recordObservable: RequestObservable<TextRecord> = {
        let request = FetchRequest<TextRecord>().sorted(with: "date", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var structureDataObservable: RequestObservable<StructuredData> = {
        let request = FetchRequest<StructuredData>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()
    
    lazy var addressObservable: RequestObservable<Address> = {
        let request = FetchRequest<Address>().sorted(with: "type", ascending: true)
        return db.observable(request: request)
    }()

    
    var textList: [TextRecord] = []
    
    var shouldReload = false
    
    var completionHandler: ((TextListViewController)->Void)?
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = true

        self.title = "Collection"
        
        self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
        self.setupObservable()
        
        let closeBtn = UIBarButtonItem(title: "Back", style: .plain , target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = closeBtn
    }
    
    @objc
    func goBack() {
        try! self.db.operation { (context, save) throws -> Void in
            save()
        }
        if let callBack = self.completionHandler {
            callBack(self)
        }
    }

    func setupObservable() {
        recordObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("text record changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        structureDataObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("structured data changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
        
        addressObservable.observe { changes in
            switch(changes) {
            case .initial:
                print("first load, do nothing")
            case .update:
                print("addresses changed, reload list")
                self.textList = try! self.db.fetch(FetchRequest<TextRecord>().sorted(with: "date", ascending: true))
                self.shouldReload = true
            case .error(let error):
                print("Something went wrong: \(error)")
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.textList.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! TextListCell
        
        let aRecord = self.textList[indexPath.row]
        DispatchQueue.main.async {
            let image = UIImage(data: aRecord.thumbNail!)
            DispatchQueue.main.async {
                cell.capturedImage.image = image
            }
        }
        cell.capturedText?.text = aRecord.content

        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let aRecord = self.textList[indexPath.row]
        let textVC = self.storyboard?.instantiateViewController(withIdentifier: "TextViewController") as! RecordViewController
        textVC.aTextRecord = aRecord
        
        textVC.cancelCompletionHandler = { (optionalRecord: TextRecord?) -> Void in
            self.dismiss(animated: true, completion: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if self.shouldReload {
                        self.tableView?.reloadData()
                    }
                })
            })
        }
        
        let navController = UINavigationController.init(rootViewController: textVC)
        
        if let aCell = tableView.cellForRow(at: indexPath) as? TextListCell {
            navController.cc_setZoomTransition(originalView: aCell.contentView)
        }
        
        self.present(navController, animated: true, completion: nil)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
 
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let record = self.textList[indexPath.item]
            do {
                try db.operation { (context, save) throws -> Void in
                    let item = try context.request(TextRecord.self).filtered(with: "content", equalTo: record.content ?? "").fetch().first
                    if let item = item {
                        self.textList.remove(at: indexPath.item)
                        try! context.remove(item)
                        tableView.deleteRows(at: [indexPath], with: .fade)
                        save()
                    }
                }
            } catch {
                print("Error deleting structured data")
            }
        }
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
