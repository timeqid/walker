//
//  SettingCell.swift
//  walker
//
//  Created by John Tran on 8/3/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit

class SettingCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.cornerRadius = 10
        self.layer.cornerRadius = 10
    }
}
