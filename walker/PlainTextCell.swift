//
//  PlainTextCell.swift
//  walker
//
//  Created by John Tran on 8/11/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit

class PlainTextCell: UITableViewCell {
    
    var aRecord: TextRecord?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
