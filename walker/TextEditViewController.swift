//
//  TextEditViewController.swift
//  walker
//
//  Created by John Tran on 8/12/17.
//  Copyright © 2017 John Tran. All rights reserved.
//

import UIKit

class TextEditViewController: UIViewController {
    
    var editCompletionHandler: ((String?, TextEditViewController)->Void)?
    var cancelCompletionHandler: ((TextEditViewController)->Void)?
    var editedText: String?
    
    @IBOutlet weak var textView: UITextView! {
        didSet {
            if textView.text.count > 0 {
                saveButton.isEnabled = true
            } else {
                saveButton.isEnabled = false
            }
        }
    }
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancel: UIBarButtonItem!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TextEditViewController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TextEditViewController.keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if let editedText = self.editedText {
            self.textView.text = editedText;
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.textView.text.count > 0 {
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.isEditing = false
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if let callBack = self.editCompletionHandler {
            callBack(self.textView.text, self)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        if let callBack = self.cancelCompletionHandler {
            callBack(self)
        }
    }
    
    
    // MARK: - notification
    
    @objc func keyboardDidShow(_ notif: NSNotification) {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let duration = notif.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
            UIView.animate(withDuration: duration.doubleValue, animations: { () -> Void in
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0)
                self.textView.contentInset = contentInsets
//                self.textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
//                self.bottomConstraint.constant -= keyboardSize.height
            })
        }
    }
    
    @objc func keyboardDidHide(_ notif: NSNotification) {
//        self.bottomConstraint.constant = 0
        let duration = notif.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        UIView.animate(withDuration: duration.doubleValue, animations: { () -> Void in
            self.textView.contentInset = UIEdgeInsets.zero
//            self.bottomConstraint.constant = 0
        })
    }
    

}
